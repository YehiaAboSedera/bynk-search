import java.io.File

import Program.Index

import scala.collection.Iterator
import scala.util.Try

class Indexer(protected val fileManager: FileManager) {

  sealed trait IndexingError

  case object EmptyFolderError extends IndexingError

  case class InvalidFolderError(t: Throwable) extends IndexingError

  def index(file: File): Either[IndexingError, Index] = {
    for {
      files <- listFilesToIndex(file)
      idx <- Right(indexFiles(files))
    } yield idx
  }

  private def listFilesToIndex(
      file: File
  ): Either[IndexingError, Array[File]] = {
    Try(fileManager.listFiles(file)).fold(
      throwable => Left(InvalidFolderError(throwable)), {
        case null => Left(EmptyFolderError)
        case fs   => Right(fs)
      }
    )
  }

  def normalizeFileContent(rawFileContent: Iterator[String]): Set[String] = {
    rawFileContent
      .map(_.trim)
      .map(_.replaceAll("\\W", " ").toLowerCase)
      .flatMap(_.split(" ").filterNot(_.isEmpty()))
      .toSet
  }

  def indexFiles(files: Array[File]): Index = {
    readAndNormalizeFiles(files)
      .foldLeft(Map.empty[String, Set[String]])(
        (acc, entry) => {
          val (file, words): (String, Set[String]) = entry
          buildFileIndex(acc)(file, words)
        }
      )
  }

  private def readAndNormalizeFiles(
      files: Array[File]
  ): Array[(String, Set[String])] = {
    println(s"Preparing to index ${files.length} files!")
    for {
      file <- files
      fileName = file.getName
      rawFileContent = fileManager.readLines(file)
      normalizedFileContent = normalizeFileContent(rawFileContent)
    } yield (fileName, normalizedFileContent)
  }

  def buildFileIndex(
      index: Map[String, Set[String]]
  )(fileName: String, words: Set[String]): Index = {
    words.foldLeft(index) { (indexAcc, word) =>
      val wordFiles = indexAcc.getOrElse(word, Set.empty) + fileName
      indexAcc.updated(word, wordFiles)
    }

  }
}

