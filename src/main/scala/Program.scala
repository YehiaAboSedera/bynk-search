import java.io.File

import scala.io.StdIn
import scala.util.Try

object Program {
  type Index = Map[String, Set[String]]
  type SearchResults = Array[(String, Double)]

  sealed trait ReadFileError
  case object MissingPathArg extends ReadFileError
  case class NotDirectory(error: String) extends ReadFileError
  case class FileNotFound(t: Throwable) extends ReadFileError


  def readFile(args: Array[String])(implicit fileManager: FileManager): Either[ReadFileError, File] = {
    for {
      path <- args.headOption.toRight(MissingPathArg)
      file <- Try(fileManager.readFile(path))
        .fold(
          throwable => Left(FileNotFound(throwable)),
          file =>
            if (file.isDirectory) Right(file)
            else Left(NotDirectory(s"Path [$path] is not a directory"))
        )
    } yield file
  }

  def iterate(
      index: Index
  )(implicit indexer: Indexer): Unit = {
    println("Search:>")
    val input = StdIn.readLine()
    handleUserInput(index, input)
  }

  private def handleUserInput(index: Index, input: String)(implicit indexer: Indexer): Unit = {
    input match {
      case ":quit" | ":q" =>
        println("Goodbye")
        sys.exit(0)
      case query =>
        println(s"searching for '$query'....")
        search(query, index)
          .foreach(r => println(f"- ${r._1}%20s : ${r._2}"))
        iterate(index)
    }
  }

  def search(query: String, index: Index)(implicit indexer: Indexer): SearchResults = {
    val queryWords = query.split(" ")
    val normalizeQuery: Set[String] = indexer
      .normalizeFileContent(queryWords.iterator)
    normalizeQuery
      .foldLeft(Map.empty[String, Int]) { (acc, e) =>
        getFilesMatchingQueries(index, acc, e)
      }
      .mapValues(_.toDouble * 100.0 / normalizeQuery.size.toDouble)
      .toArray
      .sortBy(-_._2)
      .take(10)
  }

  private def getFilesMatchingQueries(index: Index,
                                      fileWoldCount: Map[String, Int],
                                      word: String) = {
    val filesContainingWord = index.getOrElse(word, Set.empty[String])
    filesContainingWord.foldLeft(fileWoldCount)((fwAcc, fileName) => {
      val cnt = fwAcc.getOrElse(fileName, 0) + 1
      fwAcc.updated(fileName, cnt)
    })
  }
}
