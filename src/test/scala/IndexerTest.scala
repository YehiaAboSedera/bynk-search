import java.io.File

import Program.Index
import org.scalatest.{BeforeAndAfterEach, FlatSpec, MustMatchers}
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito.{times, verify, when, reset}
import org.mockito.ArgumentMatchers.any

class IndexerTest
    extends FlatSpec
    with MustMatchers
    with MockitoSugar
    with BeforeAndAfterEach {

  val fileManager: FileManager = mock[FileManager]
  val indexer: Indexer = new Indexer(fileManager)

  override def beforeEach(): Unit = {
    reset(fileManager)
  }
  "IndexerTest.normalizeFileContent" should " return a set of unique alpha-numeric strings" in {
    val array: Array[String] =
      Array("Testing String   ", "   aNother_testing-string", "$!@#^()*&%%^")
    val normalizedWords = Set("testing", "string", "another_testing")
    val result: Set[String] = indexer.normalizeFileContent(array.iterator)
    result must contain theSameElementsAs normalizedWords
  }

  "IndexerTest.indexFiles" should "return word index of all the valid content of the given files" in {
    val fileContent: Array[String] = Array("Testing", "String", "123")

    val file1 = new File("/some/path/to/file1.txt")
    val file2 = new File("/some/path/to/file2.txt")

    when(fileManager.readLines(file1)).thenReturn(fileContent.iterator)
    when(fileManager.readLines(file2)).thenReturn(fileContent.reverseIterator)

    val index: Index = indexer.indexFiles(Array(file1, file2))

    index must contain theSameElementsAs Map(
      "testing" -> Set("file1.txt", "file2.txt"),
      "string" -> Set("file1.txt", "file2.txt"),
      "123" -> Set("file1.txt", "file2.txt")
    )

    verify(fileManager, times(2)).readLines(any[File])
  }
  "IndexerTest.indexFiles" should "return empty word index " in {
    val fileContent: Array[String] = Array("!@#$%$%^")

    val file1 = new File("/some/path/to/file1.txt")
    val file2 = new File("/some/path/to/file2.txt")

    when(fileManager.readLines(file1)).thenReturn(fileContent.iterator)
    when(fileManager.readLines(file2)).thenReturn(fileContent.reverseIterator)

    val index: Index = indexer.indexFiles(Array(file1, file2))

    index must be(empty)

    verify(fileManager, times(2)).readLines(any[File])
  }

}
