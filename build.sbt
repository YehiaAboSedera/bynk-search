scalaVersion := "2.12.8"
name := "bynk-search"
organization := "io.github.ysedira"
version := "1.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test
libraryDependencies += "org.mockito" % "mockito-scala_2.12" % "1.5.16"
