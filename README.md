## Assumption 
- The current implementation only consider alpha-numeric values, it normalize both file content and query to accept only alphnumeric characters (e.g. test-string => test string, 123_456 => 123_456, !@#$%^&*() => ) 
- Match is case insensitive (e.g. abcde == ABCDE == AbCdE)
- No partial word match, files will only match if the exact work is found (e.g if the file content is  "some books" and the query is "book" file won't be considered as a match.)
- Ranking of match is based on the follwoing formula = # matching words in file / # of words in the query   
## Runing the application 
```
sbt runMain Main <path/to/dir/with/files/to/index>
...
Search:> 
<query> 
```
## Runing test 
```
sbt test
```