import java.io.File

import scala.io.Source

class FileManager {
  def listFiles(file: File): Array[File] = {
    file.listFiles()
  }


  def readLines(file: File): Iterator[String] = {
    val source = Source.fromFile(file)
    source
      .getLines()
  }

  def readFile(filePath: String): File = {
    new File(filePath)
  }
}