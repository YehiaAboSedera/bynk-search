

object Main extends App {
  implicit val fileManager: FileManager = new FileManager
  implicit val indexer: Indexer = new Indexer(fileManager)

  Program
    .readFile(args)
    .map(indexer.index)
    .fold(println, {
      case Left(value)  => println(value)
      case Right(index) => Program.iterate(index)
    })

}
