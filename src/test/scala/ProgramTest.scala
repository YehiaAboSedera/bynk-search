import Program.{Index, SearchResults}
import org.mockito.Mockito.{reset, when}
import org.mockito.ArgumentMatchersSugar.{any}
import org.scalatest.{BeforeAndAfterEach, FlatSpec, MustMatchers}
import org.scalatestplus.mockito.MockitoSugar
class ProgramTest
    extends FlatSpec
    with MustMatchers
    with MockitoSugar
    with BeforeAndAfterEach {
  private implicit val fileManager: FileManager = mock[FileManager]
  private implicit val indexer: Indexer = mock[Indexer]
  override def beforeEach(): Unit = {
    reset(indexer)
    reset(fileManager)
    when(indexer.normalizeFileContent(any[Iterator[String]]))
      .thenCallRealMethod()
  }
  "Program.search" should "return the top 10 file matching the given query sorted in descending order " in {
    val index: Index = Map(
      "testing" -> (1 to 10).map(x => f"file$x.txt").toSet,
      "string" -> Set("file1.txt", "file2.txt"),
      "123" -> Set("file1.txt", "file2.txt")
    )
    val results: SearchResults = Program.search("testing-string", index)
    results must contain theSameElementsAs Array(
      ("file1.txt", 100.0),
      ("file2.txt", 100.0),
      ("file3.txt", 50.0),
      ("file4.txt", 50.0),
      ("file5.txt", 50.0),
      ("file6.txt", 50.0),
      ("file7.txt", 50.0),
      ("file8.txt", 50.0),
      ("file9.txt", 50.0),
      ("file10.txt", 50.0)
    )
  }
  "Program.search" should "the top 10 file matching ignoreing invalid strings" in {
    val index: Index = Map(
      "testing" -> (1 to 10).map(x => f"file$x.txt").toSet,
      "string" -> Set("file1.txt", "file2.txt"),
      "123" -> Set("file1.txt", "file2.txt")
    )
    val results: SearchResults = Program.search("something else", index)
    results must be(empty)
  }
  "Program.search" should "return empty array in case no files contain any of  the given query  " in {
    val index: Index = Map(
      "testing" -> (1 to 10).map(x => f"file$x.txt").toSet,
      "string" -> Set("file1.txt", "file2.txt"),
      "123" -> Set("file1.txt", "file2.txt")
    )
    val results: SearchResults = Program.search("string !@#%%^^^", index)
    results must contain theSameElementsAs Array(
      ("file1.txt", 100.0),
      ("file2.txt", 100.0)
    )
  }
}
